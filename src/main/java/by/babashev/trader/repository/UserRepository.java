package by.babashev.trader.repository;

import by.babashev.trader.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users u WHERE u.name = :userName", nativeQuery = true)
    Optional<User> findUserByUserName(@Param("userName") String userName);
}
