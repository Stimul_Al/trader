package by.babashev.trader.controller;

import by.babashev.trader.dto.game_object.GameObjectCreateDto;
import by.babashev.trader.dto.game_object.GameObjectFullDto;
import by.babashev.trader.dto.game_object.GameObjectPreviewDto;
import by.babashev.trader.dto.game_object.GameObjectUpdateDto;
import by.babashev.trader.service.GameObjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/trader/gameObject")
public class GameObjectController {

    private final GameObjectService gameObjectService;

    @GetMapping("/{id}")
    public GameObjectFullDto findById(@PathVariable Long id){
        return gameObjectService.findById(id);
    }

    @GetMapping
    public List<GameObjectPreviewDto> findAll() {
        return gameObjectService.findAll();
    }

    @PostMapping
    public ResponseEntity<GameObjectFullDto> createGameObject(@RequestBody GameObjectCreateDto createDto) {
        return new ResponseEntity(gameObjectService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    public GameObjectFullDto updateGameObject(@RequestBody GameObjectUpdateDto updateDto) {
        return gameObjectService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        gameObjectService.deleteById(id);
    }
}
