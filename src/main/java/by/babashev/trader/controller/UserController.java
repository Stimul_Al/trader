package by.babashev.trader.controller;

import by.babashev.trader.dto.user.UserCreateDto;
import by.babashev.trader.dto.user.UserFullDto;
import by.babashev.trader.dto.user.UserPreviewDto;
import by.babashev.trader.dto.user.UserUpdateDto;
import by.babashev.trader.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/trader")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/{id}")
    public UserFullDto findById(@PathVariable Long id){
        return userService.findById(id);
    }

    @GetMapping
    public List<UserPreviewDto> findAll() {
        return userService.findAll();
    }

    @PostMapping
    public ResponseEntity<UserFullDto> createUser(@RequestBody UserCreateDto createDto) {
        return new ResponseEntity(userService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    public UserFullDto updateUser(@RequestBody UserUpdateDto updateDto) {
        return userService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        userService.deleteById(id);
    }
}
