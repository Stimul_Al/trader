package by.babashev.trader.controller;

import by.babashev.trader.dto.comment.CommentCreateDto;
import by.babashev.trader.dto.comment.CommentFullDto;
import by.babashev.trader.dto.comment.CommentPreviewDto;
import by.babashev.trader.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/trader/comment")
public class CommentController {

    private final CommentService commentService;

    @GetMapping("/{id}")
    public CommentFullDto findById(@PathVariable Long id){
        return commentService.findById(id);
    }

    @GetMapping
    public List<CommentPreviewDto> findAll() {
        return commentService.findAll();
    }

    @PostMapping
    public ResponseEntity<CommentFullDto> createComment(@RequestBody CommentCreateDto createDto) {
        return new ResponseEntity(commentService.create(createDto), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        commentService.deleteById(id);
    }
}
