package by.babashev.trader.controller;

import by.babashev.trader.dto.post.PostCreateDto;
import by.babashev.trader.dto.post.PostFullDto;
import by.babashev.trader.dto.post.PostPreviewDto;
import by.babashev.trader.dto.post.PostUpdateDto;
import by.babashev.trader.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/trader/post")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ALL')")
    public PostFullDto findById(@PathVariable Long id){
        return postService.findById(id);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ALL')")
    public List<PostPreviewDto> findAll() {
        return postService.findAll();
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_TRADER')")
    public ResponseEntity<PostFullDto> createPost(@RequestBody PostCreateDto createDto) {
        return new ResponseEntity(postService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_TRADER', 'ROLE_ADMIN')")
    public PostFullDto updatePost(@RequestBody PostUpdateDto updateDto) {
        return postService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TRADER')")
    public void deleteById(@PathVariable Long id) {
        postService.deleteById(id);
    }
}
