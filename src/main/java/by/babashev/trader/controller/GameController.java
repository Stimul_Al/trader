package by.babashev.trader.controller;

import by.babashev.trader.dto.game.GameCreateDto;
import by.babashev.trader.dto.game.GameFullDto;
import by.babashev.trader.dto.game.GamePreviewDto;
import by.babashev.trader.dto.game.GameUpdateDto;
import by.babashev.trader.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/trader/game")
public class GameController {

    private final GameService gameService;

    @GetMapping("/{id}")
    public GameFullDto findById(@PathVariable Long id){
        return gameService.findById(id);
    }

    @GetMapping
    public List<GamePreviewDto> findAll() {
        return gameService.findAll();
    }

    @PostMapping
    public ResponseEntity<GameFullDto> createGame(@RequestBody GameCreateDto createDto) {
        return new ResponseEntity(gameService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    public GameFullDto updateGame(@RequestBody GameUpdateDto updateDto) {
        return gameService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        gameService.deleteById(id);
    }
}
