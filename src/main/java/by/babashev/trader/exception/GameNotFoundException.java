package by.babashev.trader.exception;

public class GameNotFoundException extends EntityNotFoundException {

    public GameNotFoundException(Long id) {
        super("Game not found by id: " + id);
    }
}
