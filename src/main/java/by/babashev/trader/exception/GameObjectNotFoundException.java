package by.babashev.trader.exception;

public class GameObjectNotFoundException extends EntityNotFoundException {

    public GameObjectNotFoundException(Long id) {
        super("GameObject not found by id: " + id);
    }
}
