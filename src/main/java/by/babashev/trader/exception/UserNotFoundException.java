package by.babashev.trader.exception;

public class UserNotFoundException extends EntityNotFoundException{

    public UserNotFoundException(Long id) {
        super("User not found by id: " + id);
    }

    public UserNotFoundException(String name) {
        super("User not found by name: " + name);
    }
}
