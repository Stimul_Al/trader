package by.babashev.trader.exception;

public class PostNotFoundException extends EntityNotFoundException{

    public PostNotFoundException(Long id) {
        super("Post not found by id: " + id);
    }
}
