package by.babashev.trader.exception;

public class CommentNotFoundException extends EntityNotFoundException{

    public CommentNotFoundException(Long id) {
        super("Comment not found by id: " + id);
    }
}
