package by.babashev.trader.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Permission {

    ROLE_ADMIN("admin"),
    ROLE_TRADER("trader"),
    ROLE_ALL("all");

    private final String role;
}
