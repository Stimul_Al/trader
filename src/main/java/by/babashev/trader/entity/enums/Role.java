package by.babashev.trader.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN(Set.of(Permission.ROLE_ADMIN, Permission.ROLE_ALL)),
    TRADER(Set.of(Permission.ROLE_TRADER, Permission.ROLE_ALL));

    private final Set<Permission> permission;

    public Set<SimpleGrantedAuthority> getGrantedAuthorize() {
        return getPermission().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getRole()))
                .collect(Collectors.toSet());
    }
}
