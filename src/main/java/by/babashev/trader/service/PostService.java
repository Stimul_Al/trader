package by.babashev.trader.service;

import by.babashev.trader.dto.post.PostCreateDto;
import by.babashev.trader.dto.post.PostFullDto;
import by.babashev.trader.dto.post.PostPreviewDto;
import by.babashev.trader.dto.post.PostUpdateDto;

import java.util.List;

public interface PostService {

    PostFullDto findById(Long id);

    List<PostPreviewDto> findAll();

    PostFullDto create(PostCreateDto createDto);

    PostFullDto update(PostUpdateDto updateDto);

    void deleteById(Long id);
}
