package by.babashev.trader.service;

import by.babashev.trader.dto.comment.CommentCreateDto;
import by.babashev.trader.dto.comment.CommentFullDto;
import by.babashev.trader.dto.comment.CommentPreviewDto;

import java.util.List;

public interface CommentService {

    CommentFullDto findById(Long id);

    List<CommentPreviewDto> findAll();

    CommentFullDto create(CommentCreateDto createDto);

    void deleteById(Long id);
}
