package by.babashev.trader.service;

import by.babashev.trader.dto.game_object.GameObjectCreateDto;
import by.babashev.trader.dto.game_object.GameObjectFullDto;
import by.babashev.trader.dto.game_object.GameObjectPreviewDto;
import by.babashev.trader.dto.game_object.GameObjectUpdateDto;

import java.util.List;

public interface GameObjectService {

    GameObjectFullDto findById(Long id);

    List<GameObjectPreviewDto> findAll();

    GameObjectFullDto create(GameObjectCreateDto createDto);

    GameObjectFullDto update(GameObjectUpdateDto updateDto);

    void deleteById(Long id);
}
