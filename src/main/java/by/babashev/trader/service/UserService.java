package by.babashev.trader.service;

import by.babashev.trader.dto.user.UserCreateDto;
import by.babashev.trader.dto.user.UserFullDto;
import by.babashev.trader.dto.user.UserPreviewDto;
import by.babashev.trader.dto.user.UserUpdateDto;

import java.util.List;

public interface UserService {

     UserFullDto findById(Long id);

     List<UserPreviewDto> findAll();

     UserFullDto create(UserCreateDto createDto);

     UserFullDto update(UserUpdateDto updateDto);

     void deleteById(Long id);
}
