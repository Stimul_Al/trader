package by.babashev.trader.service;

import by.babashev.trader.dto.game.GameCreateDto;
import by.babashev.trader.dto.game.GameFullDto;
import by.babashev.trader.dto.game.GamePreviewDto;
import by.babashev.trader.dto.game.GameUpdateDto;

import java.util.List;

public interface GameService {

    GameFullDto findById(Long id);

    List<GamePreviewDto> findAll();

    GameFullDto create(GameCreateDto createDto);

    GameFullDto update(GameUpdateDto updateDto);

    void deleteById(Long id);
}
