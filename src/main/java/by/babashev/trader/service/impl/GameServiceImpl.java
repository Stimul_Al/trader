package by.babashev.trader.service.impl;

import by.babashev.trader.dto.game.GameCreateDto;
import by.babashev.trader.dto.game.GameFullDto;
import by.babashev.trader.dto.game.GamePreviewDto;
import by.babashev.trader.dto.game.GameUpdateDto;
import by.babashev.trader.entity.Game;
import by.babashev.trader.exception.CommentNotFoundException;
import by.babashev.trader.exception.GameNotFoundException;
import by.babashev.trader.exception.GameObjectNotFoundException;
import by.babashev.trader.repository.GameRepository;
import by.babashev.trader.service.GameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.trader.mapper.GameMapper.GAME_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;

    @Override
    public GameFullDto findById(Long id) {
        GameFullDto findGame = gameRepository.findById(id)
                .map(GAME_MAPPER::mapToFullDto)
                .orElseThrow(() -> new GameNotFoundException(id));

        log.info("GameServiceImpl -> find game by id: " + id);
        return findGame;
    }

    @Override
    public List<GamePreviewDto> findAll() {
        List<GamePreviewDto> findGames =
                gameRepository.findAll().stream()
                        .map(GAME_MAPPER::mapToPreviewDto)
                        .collect(Collectors.toList());

        log.info("GameServiceImpl -> find games. Count: " + findGames.size());
        return findGames;
    }

    @Override
    public GameFullDto create(GameCreateDto createDto) {
        Game gameToCreate = GAME_MAPPER.mapToEntity(createDto);

        Game savedGame = gameRepository.save(gameToCreate);

        log.info("GameServiceImpl -> create game by id: " + savedGame.getId());
        return GAME_MAPPER.mapToFullDto(savedGame);
    }

    @Override
    public GameFullDto update(GameUpdateDto updateDto) {
        Game existingGame = gameRepository.findById(updateDto.getId())
                .orElseThrow(() -> new GameObjectNotFoundException(updateDto.getId()));

        existingGame.setName(updateDto.getName());
        Game updatedGame = gameRepository.save(existingGame);

        log.info("GameServiceImpl -> create game by id: " + updatedGame.getId());
        return GAME_MAPPER.mapToFullDto(updatedGame);
    }

    @Override
    public void deleteById(Long id) {
        if (!gameRepository.existsById(id)) {
            throw new CommentNotFoundException(id);
        }

        gameRepository.deleteById(id);
    }
}
