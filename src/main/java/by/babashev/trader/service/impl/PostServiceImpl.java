package by.babashev.trader.service.impl;

import by.babashev.trader.dto.post.PostCreateDto;
import by.babashev.trader.dto.post.PostFullDto;
import by.babashev.trader.dto.post.PostPreviewDto;
import by.babashev.trader.dto.post.PostUpdateDto;
import by.babashev.trader.entity.GameObject;
import by.babashev.trader.entity.Post;
import by.babashev.trader.entity.User;
import by.babashev.trader.exception.GameNotFoundException;
import by.babashev.trader.exception.GameObjectNotFoundException;
import by.babashev.trader.exception.PostNotFoundException;
import by.babashev.trader.exception.UserNotFoundException;
import by.babashev.trader.repository.GameObjectRepository;
import by.babashev.trader.repository.PostRepository;
import by.babashev.trader.repository.UserRepository;
import by.babashev.trader.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.trader.mapper.PostMapper.POST_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final GameObjectRepository gameObjectRepository;
    private final UserRepository userRepository;

    @Override
    public PostFullDto findById(Long id) {
        PostFullDto findPost = postRepository.findById(id)
                .map(POST_MAPPER::mapToFullDto)
                .orElseThrow(() -> new PostNotFoundException(id));

        log.info("PostServiceImpl -> find post by id: " + id);
        return findPost;
    }

    @Override
    public List<PostPreviewDto> findAll() {
        List<PostPreviewDto> findPosts =
                postRepository.findAll().stream()
                        .map(POST_MAPPER::mapToPreviewDto)
                        .collect(Collectors.toList());

        log.info("PostServiceIpl -> find posts. Count:" + findPosts.size());
        return findPosts;
    }

    @Override
    public PostFullDto create(PostCreateDto createDto) {
        Post postToCreate = POST_MAPPER.mapToEntity(createDto);
        GameObject existingPost = gameObjectRepository.findById(createDto.getGameObjectId())
                .orElseThrow(() -> new GameObjectNotFoundException(createDto.getGameObjectId()));

        User existingUser = userRepository.findById(createDto.getUserId())
                .orElseThrow(() -> new UserNotFoundException(createDto.getUserId()));

        postToCreate.setGameObject(existingPost);
        postToCreate.setUser(existingUser);

        Post savedPost = postRepository.save(postToCreate);

        log.info("PostServiceImpl -> post create by id:" + savedPost.getId());
        return POST_MAPPER.mapToFullDto(savedPost);
    }

    @Override
    public PostFullDto update(PostUpdateDto updateDto) {
        Post existingPost = postRepository.findById(updateDto.getId())
                .orElseThrow(() -> new PostNotFoundException(updateDto.getId()));

        existingPost.setMessage(updateDto.getMessage());
        existingPost.setTitle(updateDto.getTitle());

        Post updatedPost = postRepository.save(existingPost);

        return POST_MAPPER.mapToFullDto(updatedPost);
    }

    @Override
    public void deleteById(Long id) {
        if (!postRepository.existsById(id)) {
            throw new PostNotFoundException(id);
        }

        postRepository.deleteById(id);
    }
}
