package by.babashev.trader.service.impl;

import by.babashev.trader.dto.game_object.GameObjectCreateDto;
import by.babashev.trader.dto.game_object.GameObjectFullDto;
import by.babashev.trader.dto.game_object.GameObjectPreviewDto;
import by.babashev.trader.dto.game_object.GameObjectUpdateDto;
import by.babashev.trader.entity.GameObject;
import by.babashev.trader.entity.enums.Status;
import by.babashev.trader.exception.GameObjectNotFoundException;
import by.babashev.trader.repository.GameObjectRepository;
import by.babashev.trader.service.GameObjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.trader.mapper.GameObjectMapper.GAME_OBJECT_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class GameObjectServiceImpl implements GameObjectService {

    private final GameObjectRepository gameObjectRepository;

    @Override
    public GameObjectFullDto findById(Long id) {
        GameObjectFullDto foundGameObject =
                gameObjectRepository.findById(id)
                        .map(GAME_OBJECT_MAPPER::mapToFullDto)
                        .orElseThrow(() -> new GameObjectNotFoundException(id));

        log.info("GameObjectImpl -> find gameObject by id:" + id);
        return foundGameObject;
    }

    @Override
    public List<GameObjectPreviewDto> findAll() {
        List<GameObjectPreviewDto> gameObjects =
                gameObjectRepository.findAll().stream()
                        .map(GAME_OBJECT_MAPPER::mapToPreviewDto)
                        .collect(Collectors.toList());

        log.info("GameObjectImpl -> find gameObjects. Count: " + gameObjects.size());
        return gameObjects;
    }

    @Override
    public GameObjectFullDto create(GameObjectCreateDto createDto) {
        GameObject gameObjectToCreate = GAME_OBJECT_MAPPER.mapToEntity(createDto);

        gameObjectToCreate.setCreateAt(Instant.now());
        gameObjectToCreate.setStatus(Status.ACTIVELY);

        GameObject savedGameObject = gameObjectRepository.save(gameObjectToCreate);

        log.info("GameObjectImpl -> create gameObject by id:" + savedGameObject.getId());
        return GAME_OBJECT_MAPPER.mapToFullDto(savedGameObject);
    }

    @Override
    public GameObjectFullDto update(GameObjectUpdateDto updateDto) {
        GameObject existingGameObject = gameObjectRepository.findById(updateDto.getId())
                .orElseThrow(() -> new GameObjectNotFoundException(updateDto.getId()));

        existingGameObject.setUpdateAt(Instant.now());
        existingGameObject.setTitle(updateDto.getTitle());
        existingGameObject.setText(updateDto.getText());
        existingGameObject.setStatus(updateDto.getStatus());

        GameObject updatedGameObject = gameObjectRepository.save(existingGameObject);

        log.info("GameObjectImpl -> update gameObject by id:" + updatedGameObject.getId());
        return GAME_OBJECT_MAPPER.mapToFullDto(updatedGameObject);
    }

    @Override
    public void deleteById(Long id) {
        if (!gameObjectRepository.existsById(id)) {
            throw new GameObjectNotFoundException(id);
        }

        gameObjectRepository.deleteById(id);
    }
}
