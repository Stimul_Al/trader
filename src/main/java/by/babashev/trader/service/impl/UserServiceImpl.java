package by.babashev.trader.service.impl;

import by.babashev.trader.dto.user.UserCreateDto;
import by.babashev.trader.dto.user.UserFullDto;
import by.babashev.trader.dto.user.UserPreviewDto;
import by.babashev.trader.dto.user.UserUpdateDto;
import by.babashev.trader.entity.User;
import by.babashev.trader.entity.enums.Role;
import by.babashev.trader.exception.UserNotFoundException;
import by.babashev.trader.repository.UserRepository;
import by.babashev.trader.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.trader.mapper.UserMapper.USER_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserFullDto findById(Long id) {
        UserFullDto foundUser = userRepository.findById(id)
                .map(USER_MAPPER::mapToFullDto)
                .orElseThrow(() -> new UserNotFoundException(id));

        log.info("UserServiceImpl -> found user by Id:" + foundUser.getId());
        return foundUser;
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserPreviewDto> findAll() {
        List<UserPreviewDto> foundUsers =
                userRepository.findAll().stream()
                        .map(USER_MAPPER::mapToPreviewDto)
                        .collect(Collectors.toList());

        log.info("UserServiceImpl -> found users. count: " + foundUsers.size());
        return foundUsers;
    }

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {
        User userToCreate = USER_MAPPER.mapToEntity(createDto);

        userToCreate.setCreate_at(Instant.now());
        userToCreate.setRole(Role.TRADER);

        User createdUser = userRepository.save(userToCreate);

        log.info("UserServiceImpl -> created user by Id:" + createdUser.getId());
        return USER_MAPPER.mapToFullDto(createdUser);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto updateDto) {
        User existingUser = userRepository.findById(updateDto.getId())
                .orElseThrow(() -> new UserNotFoundException(updateDto.getId()));


        existingUser.setUserName(updateDto.getUserName());
        existingUser.setRole(updateDto.getRole());

        User updatedUser = userRepository.save(existingUser);

        log.info("UserServiceImpl -> updated user by Id:" + updatedUser.getId());
        return USER_MAPPER.mapToFullDto(updatedUser);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (userRepository.existsById(id)) {
            throw new UserNotFoundException(id);
        }
        userRepository.deleteById(id);
    }
}
