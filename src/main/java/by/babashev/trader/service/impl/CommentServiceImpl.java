package by.babashev.trader.service.impl;

import by.babashev.trader.dto.comment.CommentCreateDto;
import by.babashev.trader.dto.comment.CommentFullDto;
import by.babashev.trader.dto.comment.CommentPreviewDto;
import by.babashev.trader.entity.Comment;
import by.babashev.trader.entity.User;
import by.babashev.trader.exception.CommentNotFoundException;
import by.babashev.trader.exception.UserNotFoundException;
import by.babashev.trader.repository.CommentRepository;
import by.babashev.trader.repository.UserRepository;
import by.babashev.trader.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.trader.mapper.CommentMapper.COMMENT_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;

    @Override
    public CommentFullDto findById(Long id) {
        CommentFullDto foundComment = commentRepository.findById(id)
                .map(COMMENT_MAPPER::mapToFullDto)
                .orElseThrow(() -> new CommentNotFoundException(id));

        log.info("CommentServiceImpl -> found comment by id: " + foundComment.getId());
        return foundComment;
    }

    @Override
    public List<CommentPreviewDto> findAll() {
        List<CommentPreviewDto> foundComments =
                commentRepository.findAll().stream()
                        .map(COMMENT_MAPPER::mapToPreviewDto)
                        .collect(Collectors.toList());

        log.info("CommentServiceImpl -> found comments. Count: " + foundComments.size());
        return foundComments;
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto) {
        Comment commentToCreate = COMMENT_MAPPER.mapToEntity(createDto);

        User user = userRepository.findById(createDto.getTraderId())
                .orElseThrow(() -> new UserNotFoundException(createDto.getTraderId()));

        commentToCreate.setCreate_at(Instant.now());
        commentToCreate.setUser(user);
        commentToCreate.setApproved(false);

        Comment savedComment = commentRepository.save(commentToCreate);

        log.info("CommentServiceImpl -> create comment by Id: " + savedComment.getId());
        return COMMENT_MAPPER.mapToFullDto(savedComment);
    }

    @Override
    public void deleteById(Long id) {
        if (!commentRepository.existsById(id)) {
            throw new CommentNotFoundException(id);
        }

        commentRepository.deleteById(id);
    }
}
