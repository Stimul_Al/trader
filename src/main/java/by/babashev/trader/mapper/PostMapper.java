package by.babashev.trader.mapper;

import by.babashev.trader.config.MapperConfig;
import by.babashev.trader.dto.post.PostCreateDto;
import by.babashev.trader.dto.post.PostFullDto;
import by.babashev.trader.dto.post.PostPreviewDto;
import by.babashev.trader.entity.Post;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapperConfig.class)
public interface PostMapper {

    PostMapper POST_MAPPER = Mappers.getMapper(PostMapper.class);

    @Mapping(target = "user", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "gameObject", ignore = true)
    Post mapToEntity(PostCreateDto createDto);

    @Mapping(target = "userId", expression = "java(post.getUser().getId())")
    @Mapping(target = "gameObjectId", expression = "java(post.getGameObject().getId())")
    PostFullDto mapToFullDto(Post post);

    @Mapping(target = "userId", expression = "java(post.getUser().getId())")
    @Mapping(target = "gameObjectId", expression = "java(post.getGameObject().getId())")
    PostPreviewDto mapToPreviewDto(Post post);

}
