package by.babashev.trader.mapper;

import by.babashev.trader.config.MapperConfig;
import by.babashev.trader.dto.comment.CommentCreateDto;
import by.babashev.trader.dto.comment.CommentFullDto;
import by.babashev.trader.dto.comment.CommentPreviewDto;
import by.babashev.trader.entity.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapperConfig.class)
public interface CommentMapper {

    CommentMapper COMMENT_MAPPER = Mappers.getMapper(CommentMapper.class);

    @Mapping(target = "traderId", expression = "java(comment.getUser().getId())")
    CommentFullDto mapToFullDto(Comment comment);

    @Mapping(target = "traderId", expression = "java(comment.getUser().getId())")
    CommentPreviewDto mapToPreviewDto(Comment comment);

    @Mapping(target = "user", ignore = true)
    @Mapping(target = "create_at", ignore = true)
    @Mapping(target = "approved", ignore = true)
    Comment mapToEntity(CommentCreateDto createDto);

}
