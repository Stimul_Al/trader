package by.babashev.trader.mapper;

import by.babashev.trader.config.MapperConfig;
import by.babashev.trader.dto.user.UserCreateDto;
import by.babashev.trader.dto.user.UserFullDto;
import by.babashev.trader.dto.user.UserPreviewDto;
import by.babashev.trader.dto.user.UserUpdateDto;
import by.babashev.trader.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapperConfig.class)
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "postIds", expression = "java(user.getPosts().stream().map(by.babashev.trader.entity.Post::getId).collect(java.util.stream.Collectors.toList()))")
    @Mapping(target = "commentIds", expression = "java(user.getPosts().stream().map(by.babashev.trader.entity.Post::getId).collect(java.util.stream.Collectors.toList()))")
    UserFullDto mapToFullDto(User user);

    @Mapping(target = "posts", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "role", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "create_at", ignore = true)
    User mapToEntity(UserCreateDto createDto);

    @Mapping(target = "posts", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "email", ignore = true)
    @Mapping(target = "create_at", ignore = true)
    User mapToEntity(UserUpdateDto updateDto);

    UserPreviewDto mapToPreviewDto(User user);
}
