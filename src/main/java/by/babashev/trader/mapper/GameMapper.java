package by.babashev.trader.mapper;

import by.babashev.trader.config.MapperConfig;
import by.babashev.trader.dto.game.GameCreateDto;
import by.babashev.trader.dto.game.GameFullDto;
import by.babashev.trader.dto.game.GamePreviewDto;
import by.babashev.trader.dto.game.GameUpdateDto;
import by.babashev.trader.entity.Game;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapperConfig.class)
public interface GameMapper {

    GameMapper GAME_MAPPER = Mappers.getMapper(GameMapper.class);

    @Mapping(target = "gameObjectIds", expression = "java(game.getGameObjects().stream().map(object -> object.getId()).collect(java.util.stream.Collectors.toList()))")
    GameFullDto mapToFullDto(Game game);

    GamePreviewDto mapToPreviewDto(Game game);

    @Mapping(target = "gameObjects", ignore = true)
    @Mapping(target = "id", ignore = true)
    Game mapToEntity(GameCreateDto createDto);

    @Mapping(target = "gameObjects", ignore = true)
    @Mapping(target = "id", ignore = true)
    Game mapToEntity(GameUpdateDto updateDto);
}
