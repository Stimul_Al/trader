package by.babashev.trader.mapper;

import by.babashev.trader.config.MapperConfig;
import by.babashev.trader.dto.game_object.GameObjectCreateDto;
import by.babashev.trader.dto.game_object.GameObjectFullDto;
import by.babashev.trader.dto.game_object.GameObjectPreviewDto;
import by.babashev.trader.dto.game_object.GameObjectUpdateDto;
import by.babashev.trader.entity.GameObject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = MapperConfig.class)
public interface GameObjectMapper {

    GameObjectMapper GAME_OBJECT_MAPPER = Mappers.getMapper(GameObjectMapper.class);

    @Mapping(target = "gameId", expression = "java(gameObject.getGame().getId())")
    GameObjectFullDto mapToFullDto(GameObject gameObject);

    GameObjectPreviewDto mapToPreviewDto(GameObject game);

    @Mapping(target = "updateAt", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "post", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "game", ignore = true)
    @Mapping(target = "createAt", ignore = true)
    GameObject mapToEntity(GameObjectCreateDto createDto);

    @Mapping(target = "post", ignore = true)
    @Mapping(target = "updateAt", ignore = true)
    @Mapping(target = "game", ignore = true)
    @Mapping(target = "createAt", ignore = true)
    GameObject mapToEntity(GameObjectUpdateDto updateDto);
}
