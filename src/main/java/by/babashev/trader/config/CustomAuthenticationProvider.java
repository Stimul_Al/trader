package by.babashev.trader.config;

import by.babashev.trader.entity.User;
import by.babashev.trader.exception.UserNotFoundException;
import by.babashev.trader.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        User findUser = userRepository.findUserByUserName(name)
                .orElseThrow(() -> new UserNotFoundException(name));

        if (findUser.getPassword().equals(password)) {
            return new UsernamePasswordAuthenticationToken(name, password, findUser.getRole().getGrantedAuthorize());
        } else {
            throw new BadCredentialsException("Wrong login or password!");
        }
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}
