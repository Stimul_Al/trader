package by.babashev.trader.dto.game_object;

import by.babashev.trader.entity.enums.Status;
import lombok.Data;

@Data
public class GameObjectUpdateDto {

    private Long id;
    private String title;
    private String text;
    private Status status;
}
