package by.babashev.trader.dto.game_object;

import by.babashev.trader.entity.enums.Status;
import lombok.Data;

import java.time.Instant;

@Data
public class GameObjectFullDto {

    private Long id;
    private String title;
    private String text;
    private Long gameId;
    private Status status;
    private Instant createAt;
    private Instant updateAt;
}
