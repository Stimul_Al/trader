package by.babashev.trader.dto.game_object;

import lombok.Data;

@Data
public class GameObjectPreviewDto {

    private Long id;
    private String title;
    private String text;
}
