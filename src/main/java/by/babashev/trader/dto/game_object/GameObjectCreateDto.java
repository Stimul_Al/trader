package by.babashev.trader.dto.game_object;

import lombok.Data;

@Data
public class GameObjectCreateDto {

    private String title;
    private String text;
    private Long gameId;
}
