package by.babashev.trader.dto.game;

import lombok.Data;

@Data
public class GameCreateDto {

    private String name;
}
