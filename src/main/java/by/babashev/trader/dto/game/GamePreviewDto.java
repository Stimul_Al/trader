package by.babashev.trader.dto.game;

import lombok.Data;

@Data
public class GamePreviewDto {

    private Long id;
    private String name;

}
