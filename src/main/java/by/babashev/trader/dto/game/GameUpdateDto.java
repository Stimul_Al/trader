package by.babashev.trader.dto.game;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class GameUpdateDto {

    private Long id;
    private String name;
    private List<Long> gameObjectIds = new ArrayList<>();
}
