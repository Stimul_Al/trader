package by.babashev.trader.dto.post;

import lombok.Data;

@Data
public class PostFullDto {

    private Long id;
    private String title;
    private String message;
    private Long userId;
    private Long gameObjectId;
}
