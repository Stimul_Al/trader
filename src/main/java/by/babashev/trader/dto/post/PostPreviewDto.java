package by.babashev.trader.dto.post;

import lombok.Data;

@Data
public class PostPreviewDto {

    private Long id;
    private String title;
    private Long userId;
    private Long gameObjectId;
}
