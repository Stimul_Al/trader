package by.babashev.trader.dto.post;

import lombok.Data;

@Data
public class PostUpdateDto {

    private Long id;
    private String title;
    private String message;
}
