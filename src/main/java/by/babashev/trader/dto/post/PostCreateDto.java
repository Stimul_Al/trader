package by.babashev.trader.dto.post;

import lombok.Data;

@Data
public class PostCreateDto {

    private String title;
    private String message;
    private Long userId;
    private Long gameObjectId;

}
