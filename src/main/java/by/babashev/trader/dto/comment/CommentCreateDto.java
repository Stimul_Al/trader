package by.babashev.trader.dto.comment;

import lombok.Data;

@Data
public class CommentCreateDto {

    private Long id;
    private String message;
    private String authorName;
    private Long traderId;
}
