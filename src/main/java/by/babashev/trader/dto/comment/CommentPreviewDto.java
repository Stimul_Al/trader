package by.babashev.trader.dto.comment;

import lombok.Data;

import java.time.Instant;

@Data
public class CommentPreviewDto {

    private Long id;
    private String message;
    private Long traderId;
    private String authorName;
    private Instant create_at;
}
