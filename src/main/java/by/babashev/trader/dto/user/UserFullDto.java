package by.babashev.trader.dto.user;

import by.babashev.trader.entity.Post;
import by.babashev.trader.entity.enums.Role;
import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {

    private Long id;
    private String userName;
    private String password;
    private String email;
    private Instant create_at;
    private Role role;
    private List<Long> postIds = new ArrayList<>();
    private List<Long> commentIds = new ArrayList<>();
}
