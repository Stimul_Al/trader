package by.babashev.trader.dto.user;

import by.babashev.trader.entity.enums.Role;
import lombok.Data;

@Data
public class UserUpdateDto {

    private Long id;
    private String userName;
    private Role role;
}
