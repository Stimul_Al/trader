package by.babashev.trader.dto.user;

import by.babashev.trader.entity.enums.Role;
import lombok.Data;

@Data
public class UserPreviewDto {

    private Long id;
    private String userName;
    private Role role;
}
