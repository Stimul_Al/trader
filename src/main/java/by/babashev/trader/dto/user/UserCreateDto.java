package by.babashev.trader.dto.user;

import lombok.Data;

@Data
public class UserCreateDto {

    private String userName;
    private String password;
    private String email;
}
